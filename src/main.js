import { createApp } from 'vue'
import { createPinia } from "pinia";
import VueFeather from 'vue-feather';
import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css';

import 'vue3-carousel/dist/carousel.css';
import App from './App.vue'
import router from "./router";
import globalComponents from "./global-components";
import utils from "./utils";
import store from "./stores";
import "./assets/css/app.css";
import "./libs";
import axios from 'axios';

const url = "http://localhost:3030/v1"
// const url = "http://10.175.11.75:82/v1"
let axiosOptoins = {
  baseURL: url,
  timeout: 600000, // 3 menit
};

const app = createApp(App).use(router).use(store).use(createPinia());
app.config.globalProperties.$axios = axios.create(axiosOptoins);
globalComponents(app);
utils(app);
app.component(VueFeather.name, VueFeather);
app.component('Datepicker', Datepicker);

app.mount("#app");