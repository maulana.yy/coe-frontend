import { defineStore } from "pinia";
export const EventBus = defineStore("notify",{
    state: () => {
        return {
          newNotify: []
        }
    },
    
    getters: {
        results(state) {
            return state.newNotify;
        },
    },
    actions: {
        setNotify(notif){
            this.newNotify = notif
        }
    }
});