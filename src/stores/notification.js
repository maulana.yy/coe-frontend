import { defineStore } from "pinia";

export const Notification = defineStore("Notification", {
    state: () => ({
        values : {},
    }),
    getters: {
        Notification(state) {
          return state.values;
        },
    },
    actions: {
        sentNotification(Notification) {
            this.values += Notification;
        },
    },
});
  