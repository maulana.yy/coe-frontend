import { createStore } from "vuex";
const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
  ? { status: { loggedIn: true }, user, role : "" }
  : { status: { loggedIn: false }, user: null ,role : ""};

const store = createStore({
  state: initialState,
  getters: {
    isAuthenticated: (state) => !!state.user,
    stateUser: (state) => state.user,
    uerID: (state) => state.user_id,
  },
  mutations: {
    setLogin(state, user) {
      state.status.loggedIn = true;
      state.user = user;
      state.role = user.role
    },
    setLogout(state) {
      state.status.loggedIn = false;
      state.user = null;
      state.role = ""
    },
  },
  actions: {
    
  },
  modules: {},
});
export default store;
