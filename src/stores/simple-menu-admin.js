import { defineStore } from "pinia";

let menu = [
  {
    icon: "HomeIcon",
    pageName: "dashboard-After-login",
    title: "Dashboard",
  },
  {
    icon: "UsersIcon",
    pageName: "data-user",
    title: "Data User",
  },
  {
    icon: "UsersIcon",
    pageName: "data-participant",
    title: "Data Participant",
  },
  {
    icon: "DatabaseIcon",
    pageName: "data-master",
    title: "Data Master",
    subMenu: [
      {
        icon: "LayersIcon",
        pageName: "data-organizer",
        title: "Data Organizer",
        ignore: true,
      },
      {
        icon: "ServerIcon",
        pageName: "data-category",
        title: "Data Category",
        ignore: true,
      },
      {
        icon: "SunIcon",
        pageName: "data-holiday",
        title: "Data Holiday",
        ignore: true,
      },
      {
        icon: "LayoutIcon",
        pageName: "data-information",
        title: "Data Information",
        ignore: true,
      },
      {
        icon: "MenuIcon",
        pageName: "data-level",
        title: "Data Level",
        ignore: true,
      },
    ],
  },
  {
    icon: "CalendarIcon",
    pageName: "data-event-master",
    title: "COE",
    subMenu: [
      {
        icon: "CalendarIcon",
        pageName: "data-calendar",
        title: "Calendar",
        ignore: true,
      },
      {
        icon: "FolderIcon",
        pageName: "data-event",
        title: "Data Event",
        ignore: true,
      },
    ],
  },
];

export const useSimpleMenuAdminStore = defineStore("simpleMenuAdmin", {
  state: () => ({
    menu: menu,
  }),
});
