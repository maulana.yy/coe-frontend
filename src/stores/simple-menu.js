 import { defineStore } from "pinia";

let menu = [
  {
    icon: "HomeIcon",
    pageName: "dashboard-After-login",
    title: "Dashboard",
  },
  {
    icon: "CalendarIcon",
    pageName: "data-event-master",
    title: "COE",
    subMenu: [
      {
        icon: "CalendarIcon",
        pageName: "data-calendar",
        title: "Calendar",
        ignore: true,
      },
      {
        icon: "FolderIcon",
        pageName: "data-event",
        title: "Data Event",
        ignore: true,
      },
    ],
  },
];

export const useSimpleMenuStore = defineStore("simpleMenu", {
  state: () => ({
    menu: menu,
  }),
});
