import {
  defineStore
} from "pinia";

export const useSideMenuStore = defineStore("sideMenu", {
  state: () => ({
    menu: [{
        icon: "HomeIcon",
        pageName: "dashboard-After-login",
        title: "Dashboard",
      },
      {
        icon: "UsersIcon",
        pageName: "data-user",
        title: "Data User",
      },
      {
        icon: "UsersIcon",
        pageName: "data-participant",
        title: "Data Participant",
      },
      {
        icon: "DatabaseIcon",
        pageName: "data-master",
        title: "Data Master",
        subMenu: [

          {
            icon: "FolderIcon",
            pageName: "data-organizer",
            title: "Data Organizer",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-category",
            title: "Data Category",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-support",
            title: "Data Support",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-holiday",
            title: "Data Holiday",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-information",
            title: "Data Information",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-level",
            title: "Data Level",
            ignore: true,
          },
        ],
      },
      {
        icon: "CalendarIcon",
        pageName: "data-event-master",
        title: "COE",
        subMenu: [{
            icon: "CalendarIcon",
            pageName: "data-calendar",
            title: "Calendar",
            ignore: true,
          },
          {
            icon: "FolderIcon",
            pageName: "data-event",
            title: "Data Event",
            ignore: true,
          },
        ]
      },
    ],
  }),
});