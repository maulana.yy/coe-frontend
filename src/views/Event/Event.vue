<template>
    <div class="content">
        <div class="intro-y flex flex-col sm:flex-row items-center my-8">
            <div class="mr-auto">
                <h2 class="text-lg font-medium mr-auto">
                    Data Event
                </h2>
            </div>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <Loader :loading="showLoader" />
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <a href="/add-event" class="btn btn-primary shadow-md mr-2">Add New Event</a>
                <a href="" @click.prevent="getReport"
                    class="btn btn-primary px-2 box text-gray-700 dark:text-gray-300 mr-2">Download Report</a>
                <div class="hidden md:block mx-auto text-gray-600">Showing 1 to {{recordsPerPage}} of {{totalRecords}}
                    entries</div>
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    <div class="w-56 relative text-gray-700 dark:text-gray-300">
                        <input type="text"  v-model="search"  class="form-control w-56 box pr-10 placeholder-theme-8"
                            placeholder="Search..." @change="getData()">
                        <vue-feather class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" type="search">
                        </vue-feather>
                    </div>
                </div>
            </div>
            <!-- BEGIN: Data List -->
            <div class="intro-y col-span-12 overflow-auto">
                <table class="table table-report -mt-2">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap">Name</th>
                            <th class="whitespace-nowrap">Category</th>
                            <th class="whitespace-nowrap">Organizer</th>
                            <th class="whitespace-nowrap">Department</th>
                            <th class="whitespace-nowrap">Event Date</th>
                            <th class="whitespace-nowrap">Status</th>
                            <th class="whitespace-nowrap">Approve</th>
                            <th class=" whitespace-nowrap">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="intro-x" v-for="event in listItems" :key="event.id">
                            <td>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">{{event.name}}</a>
                            </td>
                            <td>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">{{event.category}}</a>
                            </td>
                            <td>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">{{event.organizer}}</a>
                            </td>
                            <td>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">{{event.department}}</a>
                            </td>
                            <td>
                                <a href="javascript:;"
                                    class="font-medium whitespace-nowrap">{{event.event_date_start}}</a>
                            </td>
                            <td
                                v-if="this.$store.state.user.user.role == 'admin' || this.$store.state.user.user.role == 'developer'">
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#status-confirmation-modal" class="btn btn-secondary w-25 "
                                    v-if="event.status == 'Scheduled'" @click="setID(event.id)">Scheduled</a>
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#status-confirmation-modal" class="btn btn-success-soft w-25 "
                                    v-else-if="event.status == 'Success'" @click="setID(event.id)">Success</a>
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#status-confirmation-modal" class="btn btn-primary w-25 "
                                    v-if="event.status == 'Reschedule'" @click="setID(event.id)">Reschedule</a>
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#status-confirmation-modal" class="btn btn-danger w-25 "
                                    v-if="event.status == 'Cancel'" @click="setID(event.id)">Cancel</a>
                            </td>
                            <td v-else>
                                <a href="javascript:;" class="font-medium whitespace-nowrap"
                                    v-if="event.status == 'Scheduled'">Scheduled</a>
                                <a href="javascript:;" class="font-medium whitespace-nowrap"
                                    v-else-if="event.status == 'Success'">Success</a>
                                <a href="javascript:;" class="font-medium whitespace-nowrap"
                                    v-if="event.status == 'Reschedule'">Reschedule</a>
                                <a href="javascript:;" class="font-medium whitespace-nowrap"
                                    v-if="event.status == 'Cancel'">Cancel</a>
                            </td>
                            <td
                                v-if="this.$store.state.user.user.role == 'admin' || this.$store.state.user.user.role == 'developer'">
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#approve-confirmation-modal" class="btn btn-success-soft w-25 "
                                    v-if="event.approved == 'APPROVED'" @click="setApprove(event.id)">{{event.approved}}</a>
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#approve-confirmation-modal" class="btn btn-danger w-25" v-else-if="event.approved == 'REJECTED'"
                                    @click="setApprove(event.id)">{{event.approved}}</a>
                                <a href="javascript:;" data-tw-toggle="modal"
                                    data-tw-target="#approve-confirmation-modal" class="btn btn-secondary w-25" v-else
                                    @click="setApprove(event.id)">{{event.approved}}</a>
                            </td>
                            <td v-else>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">{{event.approved}}</a>
                            </td>
                            <td class="table-report__action w-56"
                                v-if="this.$store.state.user.user.role == 'admin' || this.$store.state.user.user.role == 'developer'">
                                <div class="flex justify-center items-center">
                                    <a class="flex items-center mr-3" :href="'/edit-event/'+event.id">
                                        <vue-feather class="w-4 h-4 mr-1" type="edit"></vue-feather> Edit
                                    </a>
                                    <a class="flex items-center text-theme-24" href="javascript:;"
                                        data-tw-toggle="modal" data-tw-target="#delete-confirmation-modal"
                                        @click="setID(event.id)">
                                        <vue-feather class="w-4 h-4 mr-1" type="trash"></vue-feather> Delete
                                    </a>
                                </div>
                            </td>
                            <td v-else>
                                <a href="javascript:;" class="font-medium whitespace-nowrap">-</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
            <!-- BEGIN: Pagination -->
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
                <Pagination v-if="listItems" :total-pages="totalPages" :per-page="recordsPerPage" :current-page="page"
                    @pagechanged="onPageChange" />

                <select class="w-20 form-select box mt-3 sm:mt-0" v-model="recordsPerPage" @change="changeLimit()">
                    <option value=10>10</option>
                    <option value=20>20</option>
                    <option value=30>30</option>
                    <option value=50>50</option>
                </select>
            </div>
            <!-- END: Pagination -->
        </div>
        <div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="p-5 text-center">
                            <vue-feather class="w-16 h-16 text-theme-24 mx-auto mt-3" type="x-circle"></vue-feather>
                            <div class="text-3xl mt-5">Are you sure?</div>
                            <div class="text-gray-600 mt-2">
                                Do you really want to delete these records?
                                <br>
                                This process cannot be undone.
                            </div>
                        </div>
                        <div class="px-5 pb-8 text-center">
                            <button type="button" data-tw-dismiss="modal"
                                class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                            <button type="button" class="btn btn-danger w-24" @click="deleteEvent">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="approve-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="p-5 text-center">
                            <vue-feather class="w-16 h-16 text-theme-24 mx-auto mt-3" type="check-circle"></vue-feather>
                            <div class="text-3xl mt-5">Are you sure?</div>
                            <div class="text-gray-600 mt-2">
                                Do you really want to Approve or Reject these event?
                            </div>
                        </div>
                        <div class="px-5 pb-8 text-center">
                            <button type="button" class="btn btn-danger w-24 mr-1" @click="approveEvent('REJECTED')">Reject</button>
                            <button type="button" class="btn btn-primary w-24" @click="approveEvent('APPROVED')">Approve</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="status-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="pl-5 pr-5 pt-2 pb-3 text-center">
                            <div class="text-2xl mt-2">Change Status</div>
                            <div class="text-gray-600 mt-2">
                                <select class="tom-select w-full" v-model="status" data-placeholder="Select Status">
                                    <option v-for="item in statuses" :value="item" :key="item">{{item}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="pl-5 pr-5 pb-2" v-show="status == 'Reschedule'">
                            <div class="text-gray-600 mt-1">
                                <label for="crud-form-1" class="form-label mt-2">Event Date</label>
                                <Datepicker v-model="event_date" class="form-control" placeholder="Event Date ..."
                                    textInput range />
                            </div>
                        </div>
                        <div class="px-5 pb-8 text-center">
                            <button type="button" data-tw-dismiss="modal"
                                class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                            <button type="button" class="btn btn-primary w-24" @click="changeStatus">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<script>
    import Pagination from '@/components/pagination/Main.vue'
    import Loader from '@/components/pagination/Loader.vue'
    import Toastify from "toastify-js";

    export default {
        name: 'DataEvent',
        components: {
            Pagination,
            Loader
        },
        data() {
            return {
                event_date: "",
                duration: "",
                showLoader: false,
                listItems: [],
                page: 1,
                totalPages: 0,
                totalRecords: 0,
                recordsPerPage: 10,
                labelApprove: "Approve",
                limit: 10,
                offset: 0,
                search: "",
                id: 0,
                edit: false,
                label: "Save",
                statuses: [
                    "Success", "Scheduled", "Reschedule", "Cancel"
                ],
                status: ""
            }
        },
        methods: {
            setID(id) {
                this.id = id
            },
            setApprove(id) {
                this.id = id
                
            },
            setPage() {
                this.getData()
            },
            changeLimit() {
                this.getData()
            },
            onPageChange(page) {
                this.page = page
                this.getData()
            },
            async getData() {
                this.showLoader = true
                const data = {
                    offset: this.page,
                    limit: this.recordsPerPage,
                    search: this.search
                }
                this.$axios.get(`/event`, {
                        params: data,
                        headers: {
                            "Token": this.$store.state.user.token,
                            "User-ID": this.$store.state.user.user_id
                        }
                    })
                    .then((response) => {
                        this.showLoader = false
                        this.listItems = response.data.data.data ? response.data.data.data : []
                        this.listItems.map(x => {

                            x.department = x.department ? x.department.toString() : ""

                            return x
                        })


                        this.totalPages = Math.ceil(response.data.data.total / this.recordsPerPage)
                        this.totalRecords = response.data.data.total
                    }).catch((error) => {
                        Toastify({
                            text: `ERROR : ${error.response.data.message}`,
                            duration: 3000,
                            newWindow: true,
                            close: true,
                            gravity: "top",
                            position: "right",
                            stopOnFocus: true,
                            className: "toastify-content",
                        }).showToast();
                    });
            },
            async deleteEvent() {
                this.$axios.delete(`/event/${this.id}/delete`, {
                    headers: {
                        "Token": this.$store.state.user.token,
                        "User-ID": this.$store.state.user.user_id
                    }
                }).then(() => {
                    Toastify({
                        text: `Delete Event Success`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                    setTimeout(function () {
                        window.location.reload()
                    }, 1000);
                }).catch((error) => {
                    Toastify({
                        text: `ERROR : ${error.response.data.message}`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                });
            },
            async approveEvent(status) {
                this.$axios.get(`/event/${this.id}/approve?status=${status}`, {
                    headers: {
                        "Token": this.$store.state.user.token,
                        "User-ID": this.$store.state.user.user_id
                    }
                }).then((res) => {
                    Toastify({
                        text: `Approve Event Success`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                    setTimeout(function () {
                        window.location.reload()
                    }, 1000);
                }).catch((error) => {
                    Toastify({
                        text: `ERROR : ${error.response.data.message}`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                });
            },
            async changeStatus() {

                let startDate, endDate, timeStart, timeEnd = ""
                if (this.event_date != "") {
                    const formStartDate = new Date(this.event_date[0])
                    const startYear = formStartDate.getFullYear()
                    const startMonth = formStartDate.getMonth() + 1 > 9 ? (formStartDate.getMonth() + 1)
                        .toString() : "0" + (formStartDate.getMonth() + 1).toString()
                    const startDay = formStartDate.getDate() + 1 > 9 ? (formStartDate.getDate() + 1).toString() :
                        "0" + (formStartDate.getDate() + 1).toString()
                    const startHour = formStartDate.getHours() < 10 ? "0" + formStartDate.getHours() : formStartDate
                        .getHours()
                    const startMinute = formStartDate.getMinutes() < 10 ? "0" + formStartDate.getMinutes() :
                        formStartDate.getMinutes()

                    startDate = `${startYear}-${startMonth}-${startDay}`
                    timeStart = `${startHour}:${startMinute}:00`


                    if (this.event_date.length > 1) {
                        const FormatendDate = new Date(this.event_date[1])
                        const endYear = FormatendDate.getFullYear()
                        const endMonth = FormatendDate.getMonth() + 1 > 9 ? (FormatendDate.getMonth() + 1)
                            .toString() :
                            "0" + (FormatendDate.getMonth() + 1).toString()
                        const endDay = FormatendDate.getDate() + 1 > 9 ? (FormatendDate.getDate() + 1).toString() :
                            "0" + (FormatendDate.getDate() + 1).toString()

                        const endHour = FormatendDate.getHours() < 10 ? "0" + FormatendDate.getHours() :
                            FormatendDate.getHours()
                        const endMinute = FormatendDate.getMinutes() ? "0" + FormatendDate.getMinutes() :
                            FormatendDate.getMinutes()
                        endDate = this.event_date.length > 1 ? `${endYear}-${endMonth}-${endDay}` : startDate
                        timeEnd = `${endHour}:${endMinute}:00`
                    } else {
                        endDate = startDate
                        timeEnd = timeStart
                    }
                }

                this.$axios.get(`/event/${this.id}/status`, {
                    params: {
                        status: this.status,
                        new_date_start: startDate,
                        new_date_end: endDate,
                        new_time_start: timeStart,
                        new_time_end: timeEnd
                    },
                    headers: {
                        "Token": this.$store.state.user.token,
                        "User-ID": this.$store.state.user.user_id
                    }
                }).then((res) => {
                    Toastify({
                        text: `Update Event Status Success`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                    setTimeout(function () {
                        window.location.reload()
                    }, 1000);
                }).catch((error) => {
                    Toastify({
                        text: `ERROR : ${error.response.data.message}`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                });
            },
            async getReport() {
                this.$axios.get("/event/report", {
                    responseType: 'blob',
                    headers: {
                        "Token": this.$store.state.user.token,
                        "User-ID": this.$store.state.user.user_id
                    }
                }).then((response) => {
                    var fileURL = window.URL.createObjectURL(new Blob([response.data]));
                    var fileLink = document.createElement('a');
                    fileLink.href = fileURL;
                    fileLink.setAttribute('download', 'Report.xlsx');
                    document.body.appendChild(fileLink);
                    fileLink.click();
                }).catch(error => {
                    Toastify({
                        text: `ERROR : ${error.response.data.message}`,
                        duration: 3000,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        stopOnFocus: true,
                        className: "toastify-content",
                    }).showToast();
                })
            }
        },
        mounted() {
            this.getData()
        }
    }
</script>