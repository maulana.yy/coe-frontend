import dom from "@left4code/tw-starter/dist/js/dom";

// Setup side menu
const findActiveMenu = (subMenu, route) => {
  // let match = false;
  
  let find = subMenu.find(item => {
    return item.pageName == route.name
  })

  let match = find ? true : false
  return match;
};

const nestedMenu = (menu, route) => {
  menu.forEach((item, key) => {
    if (typeof item !== "string") {
      let menuItem = menu[key];

      if (item.subMenu){
        menuItem.activeDropdown = findActiveMenu(item.subMenu, route);
        menuItem.active = findActiveMenu(item.subMenu, route)

        
        menuItem = {
          ...item,
          ...nestedMenu(item.subMenu, route),
        };
      }else{
        menuItem.active = item.pageName === route.name
      }
    }
  });

  return menu;
};

const linkTo = (menu, router, event) => {
  if (menu.subMenu) {
    menu.activeDropdown = !menu.activeDropdown;
  } else {
    event.preventDefault();
    router.push({
      name: menu.pageName,
    });
  }
};

const enter = (el, done) => {
  dom(el).slideDown(300);
};

const leave = (el, done) => {
  dom(el).slideUp(300);
};

export { nestedMenu, linkTo, enter, leave };
