import {
  createRouter,
  createWebHistory
} from "vue-router";
import SideMenu from "../layouts/side-menu/Main.vue";
import SimpleMenu from "../layouts/simple-menu/Main.vue";
import TopMenu from "../layouts/top-menu/Main.vue";
import DashboardBeforeLogin from "../views/dashboard/BeforeLogin.vue";
import DashboardAfterLogin from "../views/dashboard/AfterLogin.vue";
import DataUser from '../views/User/DataUser.vue'
import FormUser from '../views/User/FormUser.vue'
import DataOrganizer from '../views/Organizer/DataOrganizer.vue'
import FormOrganizer from '../views/Organizer/FormOrganizer.vue'
import DataRole from '../views/Role/DataRole.vue'
import DataCategory from '../views/Category/DataCategory.vue'
import FormCategory from '../views/Category/FormCategory.vue'
import DataHoliday from '../views/Holiday/DataHoliday.vue'
import FormHoliday from '../views/Holiday/FormHoliday.vue'
import DataSupport from '../views/Support/DataSupport.vue'
import DataEvent from '../views/Event/Event.vue'
import DataCalendar from '../views/Event/Calendar.vue'
import FormEvent from '../views/Event/FormEvent.vue'
import DataInformation from '../views/Information/DataInformation.vue'
import DataParticipant from '../views/Participant/DataParticipant.vue'
import FormParticipant from '../views/Participant/FormParticipant.vue'
import store from '../stores'

const routes = [{
    path: "/",
    component: TopMenu,
    children: [{
      path: "/",
      name: "dashboard-before-login",
      component: DashboardBeforeLogin,
    }, ],
  },
  {
    path: "/",
    component: SimpleMenu,
    children: [{
        path: "dashboard",
        name: "dashboard-After-login",
        component: DashboardAfterLogin,
        props: true,
      },
      {
        path: "user",
        name: 'data-user',
        component: DataUser,
      },
      {
        path: "add-user",
        name: 'add-user',
        component: FormUser,
      },
      {
        path: "edit-user/:id",
        name: 'edit-user',
        component: FormUser,
      },
      {
        path: "participant",
        name: 'data-participant',
        component: DataParticipant,
      },
      {
        path: "add-participant",
        name: 'add-participant',
        component: FormParticipant,
      },
      {
        path: "edit-participant/:id",
        name: 'edit-participant',
        component: FormParticipant,
      },
      {
        path: "organizer",
        name: 'data-organizer',
        component: DataOrganizer
      },
      {
        path: "add-organizer",
        name: 'add-organizer',
        component: FormOrganizer
      },
      {
        path: "edit-organizer/:id",
        name: 'edit-organizer',
        component: FormOrganizer
      },
      {
        path: "level",
        name: 'data-level',
        component: DataRole
      },
      {
        path: "category",
        name: 'data-category',
        component: DataCategory
      },
      {
        path: "add-category",
        name: 'add-category',
        component: FormCategory
      },
      {
        path: "edit-category/:id",
        name: 'edit-category',
        component: FormCategory
      },
      {
        path: "support",
        name: 'data-support',
        component: DataSupport
      },
      {
        path: "holiday",
        name: 'data-holiday',
        component: DataHoliday
      },
      {
        path: "add-holiday",
        name: 'add-holiday',
        component: FormHoliday
      },
      {
        path: "edit-holiday/:id",
        name: 'edit-holiday',
        component: FormHoliday
      },
      {
        path: "event",
        name: 'data-event',
        component: DataEvent
      },
      {
        path: "add-event",
        name: 'add-event',
        component: FormEvent
      },
      {
        path: "edit-event/:id",
        name: 'edit-event',
        component: FormEvent
      },
      {
        path: "calendar",
        name: 'data-calendar',
        component: DataCalendar
      },
      {
        path: "information",
        name: 'data-information',
        component: DataInformation
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || {
      left: 0,
      top: 0
    };
  },
});

router.beforeEach((to, from, next) => {
  const publicPages = ['/'];
  const authRequired = !publicPages.includes(to.path);
  const notAuthRequired = publicPages.includes(to.path);
  const loggedIn = store.state.status.loggedIn
  
  if (authRequired && !loggedIn) {
    next('/');
  } else {
    next();
  }
});

export default router;