# Project COE Kalbe Morinaga Indonesia

Mengunakan beberapa plugin atau beberapa vendor
- [Vue](https://v3.vuejs.org/)
- [Vite](https://vitejs.dev)
- [Pinia](https://pinia.vuejs.org/)
- [CalenderJs](https://fullcalendar.io)
- [tailwindcss](https://tailwindcss.com/)
- [Vuex](https://vuex.vuejs.org/)

## Run Locally

Clone the project

```bash
  git clone https://github.com/danamulyana/coe-project-vite.git
```

Go to the project directory

```bash
  cd coe-project-vite
```

Install dependencies

```bash
  npm install
  or
  yarn
```

Start the server

```bash
  npm run dev
  or
  yarn dev
```

## Authors
- front end : [Dana Mulyana](https://github.com/danamulyana)
- Back end : [Maulana Yusuf](https://github.com/maulanayy)

## Tech Stack

**Client:** Vue, Vuex, Vite, Pinia, TailwindCSS

**Server:** Node, Express